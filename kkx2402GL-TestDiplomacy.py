#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy 
# -----------

class TestCollatz (TestCase):

# **Results** Check Swaps 
# A SanAntonio
# B Austin 
    def test_solve1(self):
        r = StringIO('A Austin Move SanAntonio\nB SanAntonio Move Austin\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A SanAntonio\nB Austin\n')   

# **Results** 
# A SanAntonio
# B Houston
# C Dallas
# D Waco 
# E Austin 
    def test_solve2(self):
        r = StringIO('A Austin Move SanAntonio\nB SanAntonio Move Houston\nC Houston Move Dallas\nD Dallas Move Waco\nE Waco Move Austin\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A SanAntonio\nB Houston\nC Dallas\nD Waco\nE Austin\n')

# **Results** 
# A [dead]
# B [dead]
    def test_solve3(self):
        r = StringIO('A Austin Hold\nB Katy Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Austin\nB Katy\n')  

# **Results** Support can't chain 
# A Austin 
# B Dallas 
# C SanAntonio 
# D [dead]
# E [dead]
# F Waco 
    def test_solve4(self):
        r = StringIO('A Austin Support B\nB Dallas Support C\nC SanAntonio Support D\nD Houston Move Lubbock\nE Lubbock Hold\nF Waco Support E')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Austin\nB Dallas\nC SanAntonio\nD [dead]\nE [dead]\nF Waco\n')  

# **Results* Support can Stack 
# A [dead]
# B Austin 
# C Katy 
# D Dallas 
# E ElPaso
# F Fredericksburg 

    def test_solve5(self):
        r = StringIO('A Austin Hold\nB Barcelona Move Austin\nC Katy Support A\nD Dallas Support B\nE ElPaso Support B\nF Fredericksburg Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Austin\nC Katy\nD Dallas\nE ElPaso\nF Fredericksburg\n')

    def test_solve6(self):
        r = StringIO('A Austin Support B\nB Katy Move Houston\nC Houston Move Austin\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Houston\nC [dead]\n')

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover """


