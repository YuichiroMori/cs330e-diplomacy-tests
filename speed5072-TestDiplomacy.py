# TestDiplomacy.py
#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import setup, fight, stats, run_game

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_setup(self):
        input_text= "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Support B"
        armies,cities = setup(input_text.splitlines())
        self.assertEqual(armies, {'A': ['A'], 'B': ['B', 'C'], 'C': ['C']})
        self.assertEqual(cities, {'Madrid': ['A', 'B'], 'London': ['C']})

    def test_setup1(self):
        input_text= "A Madrid Hold\r\nB Barcelona Move Madrid"
        armies,cities = setup(input_text.splitlines())
        self.assertEqual(armies, {'A': ['A'], 'B': ['B']})
        self.assertEqual(cities, {'Madrid': ['A', 'B']})
    
    def test_setup2(self):
        input_text= "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Support B\r\nD Austin Move London"
        armies,cities = setup(input_text.splitlines())
        self.assertEqual(armies, {'A': ['A'], 'B': ['B', 'C'], 'C': ['C'], 'D': ['D']})
        self.assertEqual(cities, {'Madrid': ['A', 'B'], 'London': ['C', 'D']})
    
    def test_setup3(self):
        input_text= "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Move Madrid"
        armies,cities = setup(input_text.splitlines())
        self.assertEqual(armies, {'A': ['A'], 'B': ['B'], 'C': ['C']})
        self.assertEqual(cities, {'Madrid': ['A', 'B', 'C']})
    
    def test_setup4(self):
        input_text= "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Move Madrid\r\nD Paris Support B"
        armies,cities = setup(input_text.splitlines())
        self.assertEqual(armies, {'A': ['A'], 'B': ['B', 'D'], 'C': ['C'], 'D': ['D']})
        self.assertEqual(cities, {'Madrid': ['A', 'B', 'C'], 'Paris': ['D']})
        
    def test_setup5(self):
        input_text= "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Move Madrid\r\nD Paris Support B\r\nE Austin Support A"
        armies,cities = setup(input_text.splitlines())
        self.assertEqual(armies, {'A': ['A', 'E'], 'B': ['B', 'D'], 'C': ['C'], 'D': ['D'], 'E': ['E']})
        self.assertEqual(cities, {'Madrid': ['A', 'B', 'C'], 'Paris': ['D'], 'Austin': ['E']})

    # ----
    # eval
    # ----
    
    def test_fight(self):
        armies = {'A': ['A'], 'B': ['B', 'C'], 'C': ['C']}
        cities = {'Madrid': ['A', 'B'], 'London': ['C']}
        fight(armies, cities)
        self.assertEqual(armies, {'A': 0, 'B': ['B', 'C'], 'C': ['C']})
        self.assertEqual(cities, {'Madrid': ['A', 'B'], 'London': ['C']})
        
    
    def test_fight_1(self):
        armies = {'A': ['A'], 'B': ['B']}
        cities = {'Madrid': ['A', 'B']}
        fight(armies, cities)
        self.assertEqual(armies, {'A': 0, 'B': 0})
        self.assertEqual(cities, {'Madrid': ['A', 'B']})
    
    def test_fight_2(self):
        armies = {'A': ['A'], 'B': ['B', 'C'], 'C': ['C'], 'D': ['D']}
        cities = {'Madrid': ['A', 'B'], 'London': ['C', 'D']}
        fight(armies, cities)
        self.assertEqual(armies, {'A': 0, 'B': 0, 'C': 0, 'D': 0})
        self.assertEqual(cities, {'Madrid': ['A', 'B'], 'London': ['C', 'D']})
    
    def test_fight_3(self):
        armies = {'A': ['A'], 'B': ['B'], 'C': ['C']}
        cities = {'Madrid': ['A', 'B', 'C']}
        fight(armies, cities)
        self.assertEqual(armies, {'A': 0, 'B': 0, 'C': 0})
        self.assertEqual(cities, {'Madrid': ['A', 'B', 'C']})
    
    def test_fight_4(self):
        armies = {'A': ['A'], 'B': ['B', 'D'], 'C': ['C'], 'D': ['D']}
        cities = {'Madrid': ['A', 'B', 'C'], 'Paris': ['D']}
        fight(armies, cities)
        self.assertEqual(armies, {'A': 0, 'B': ['B', 'D'], 'C': 0, 'D': ['D']})
        self.assertEqual(cities, {'Madrid': ['A', 'B', 'C'], 'Paris': ['D']})
    
    def test_fight_5(self):
        armies = {'A': ['A', 'E'], 'B': ['B', 'D'], 'C': ['C'], 'D': ['D'], 'E': ['E']}
        cities = {'Madrid': ['A', 'B', 'C'], 'Paris': ['D'], 'Austin': ['E']}
        fight(armies, cities)
        self.assertEqual(armies, {'A': 0, 'B': 0, 'C': 0, 'D': ['D'], 'E': ['E']})
        self.assertEqual(cities, {'Madrid': ['A', 'B', 'C'], 'Paris': ['D'], 'Austin': ['E']})

    # -----
    # print
    # -----
    

    
    def test_stats(self):
        w = StringIO()
        armies = {'A': 0, 'B': ['B', 'C'], 'C': ['C']}
        cities = {'Madrid': ['A', 'B'], 'London': ['C']}
        stats(w,armies,cities)
        self.assertEqual(w.getvalue(), "A [dead] \nB Madrid \nC London \n")
        
    def test_stats1(self):
        w = StringIO()
        armies = {'A': 0, 'B': 0}
        cities = {'Madrid': ['A', 'B']}
        stats(w,armies,cities)
        self.assertEqual(w.getvalue(), "A [dead] \nB [dead] \n")
    
    def test_stats2(self):
        w = StringIO()
        armies = {'A': 0, 'B': 0, 'C': 0, 'D': 0}
        cities = {'Madrid': ['A', 'B'], 'London': ['C', 'D']}
        stats(w,armies,cities)
        self.assertEqual(w.getvalue(), "A [dead] \nB [dead] \nC [dead] \nD [dead] \n")
    
    def test_stats3(self):
        w = StringIO()
        armies = {'A': 0, 'B': 0, 'C': 0}
        cities = {'Madrid': ['A', 'B', 'C']}
        stats(w, armies,cities)
        self.assertEqual(w.getvalue(), "A [dead] \nB [dead] \nC [dead] \n")
    
    def test_stats4(self):
        w = StringIO()
        armies = {'A': 0, 'B': ['B', 'D'], 'C': 0, 'D': ['D']}
        cities = {'Madrid': ['A', 'B', 'C'], 'Paris': ['D']}
        stats(w,armies,cities)
        self.assertEqual(w.getvalue(), "A [dead] \nB Madrid \nC [dead] \nD Paris \n")
    
    def test_stats5(self):
        w = StringIO()
        armies = {'A': 0, 'B': 0, 'C': 0, 'D': ['D'], 'E': ['E']}
        cities = {'Madrid': ['A', 'B', 'C'], 'Paris': ['D'], 'Austin': ['E']}
        stats(w,armies,cities)
        self.assertEqual(w.getvalue(), "A [dead] \nB [dead] \nC [dead] \nD Paris \nE Austin \n")

    

    # -----
    # solve
    # -----

    def test_run(self):
        r = "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Support B"
        w = StringIO()
        run_game(r.splitlines(), w)
        self.assertEqual(w.getvalue(), "A [dead] \nB Madrid \nC London \n")
    
      
    def test_run1(self):
        r = "A Madrid Hold\r\nB Barcelona Move Madrid"
        w = StringIO()
        run_game(r.splitlines(), w)
        self.assertEqual(
            w.getvalue(), "A [dead] \nB [dead] \n")

    def test_run2(self):
        r = "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Support B\r\nD Austin Move London"
        w = StringIO()
        run_game(r.splitlines(), w)
        self.assertEqual(
            w.getvalue(), "A [dead] \nB [dead] \nC [dead] \nD [dead] \n")

    def test_run3(self):
        r = "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Move Madrid"
        w = StringIO()
        run_game(r.splitlines(), w)
        self.assertEqual(
            w.getvalue(), "A [dead] \nB [dead] \nC [dead] \n")

    def test_run4(self):
        r = "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Move Madrid\r\nD Paris Support B"
        w = StringIO()
        run_game(r.splitlines(), w)
        self.assertEqual(
            w.getvalue(), "A [dead] \nB Madrid \nC [dead] \nD Paris \n")
    
    def test_run5(self):
        r = "A Madrid Hold\r\nB Barcelona Move Madrid\r\nC London Move Madrid\r\nD Paris Support B\r\nE Austin Support A"
        w = StringIO()
        run_game(r.splitlines(), w)
        self.assertEqual(
            w.getvalue(), "A [dead] \nB [dead] \nC [dead] \nD Paris \nE Austin \n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()


""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
